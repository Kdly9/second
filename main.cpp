#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

struct Information
{
  int requests;
  int hour;
  int summ;
  std::string info;
};

int main() {
  setlocale(LC_ALL, "Russian");

  std::ifstream infile("Input.txt");
  Information data;
  std::string line, name, requests, hour;
  std::map<std::string, Information> dataMap;
  if (infile.is_open()) {
    while (std::getline(infile, line)) {
      std::stringstream iss(line);
      if (std::getline(iss, name, ' ') &&
        std::getline(iss, requests, ' ') &&
        std::getline(iss, hour, ' ') &&
        std::getline(iss, data.info)) {
        if (dataMap.empty() || dataMap.count(name) == 0) {
          data.hour = std::stoi(hour);
          data.requests = std::stoi(requests);
          data.summ = std::stoi(requests) * std::stoi(hour);
          dataMap.insert({ name, data });
        }
        else {
          std::map<std::string, Information>::iterator it = dataMap.find(name);
          if (it != dataMap.end()) {
            it->second.hour += std::stoi(hour);
            it->second.requests += std::stoi(requests);
            it->second.summ += std::stoi(requests) * std::stoi(hour);
          }
        }
      }
      else {
        std::cout << "Read error" << std::endl;
        infile.setstate(std::ios::failbit);
      }
    }
  }
  else {
    std::cout << "File is not open" << std::endl;
  }
  infile.close();
  std::ofstream fout;
  fout.open("Output.txt");
  if (fout.is_open()) {
    for (auto out : dataMap) {
      fout << out.first << ' ' << out.second.requests << " "
        << out.second.hour << " " << out.second.summ << ' ' << " " << out.second.info << '\n';
    }
  }
  else {
    std::cout << "Unable to create or open a file  for writing" << std::endl;
  }
  
  fout.close();
  return 0;
}